App = {
  web3Provider: null,
  contracts: {},
  account: '0x0',
  

  init: function() {
    return App.initWeb3();
  },
//carga web3
  initWeb3: function() {
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      web3 = new Web3(App.web3Provider);
    }
    return App.initContract();
  },
//carga el contrato y la cuenta
  initContract: function() {
    $.getJSON("Historial.json", function(historial) {
      // Instancia un nuevo contrato con truffle
      App.contracts.Historial = TruffleContract(historial);
      // Conecta con el proveedor para acceder a la cuenta de ethereum
      App.contracts.Historial.setProvider(App.web3Provider);

      return App.render();
    });
  },
//Se encarga de cargar los datos en cada pagina, se llama siempre al principio
  render: function() {

    var loader = $("#loader");
    var content = $("#content");

    loader.show();
    content.hide();

    //carga de los datos de la cuenta
    web3.eth.getCoinbase(function(err, account) {
      if (err === null) {
        App.account = account;
        $("#accountAddress").html("Su cuenta: " + account);
        if (account!=null) {
          App.getQuienEs();
          loader.remove();
          content.show();
          if (window.location.pathname==ipfs+"/paciente.html") {
            App.getPacientesP();
            App.getHistorialP();
          }
          else if(window.location.pathname==ipfs+"/icontra.html"){
            App.getPacientePass();
          }
          else if(window.location.pathname==ipfs+"/medico.html"){
            App.getPacientesM();
          }
          else if (window.location.pathname==ipfs+"/medico_Historial.html") {
            var results = App.params("i");
            App.getPaciente(results[1]);
            App.getHistorial(results[1]);
            
          }
        }
      }
    }); 
  },
//Recoge el valor del parametro que se escriban en la url(GET)
  params: function(name){
    var results= new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results;
  },
//Permite que con cualquienr cuenta uno se registre como paciente introduciendo un nombre y la fecha de nacimiento. El metodo le añadira una contraseña de 10 caracteres. Se registrará si no esta registrado ya como paciente ni medico
  registerP: function (nombre,fechan) {
      if (nombre.length>0 && fechan.length==10) {
        var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ2346789?_¿-!.:,;ç*+";
        var contra = "";
        for (i=0; i<10; i++) {
          contra += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
        }
        try{
          App.contracts.Historial.deployed().then(function(instance){instance.addPaciente(nombre,fechan,contra).then(function(){window.location.replace(ipfs+"/");})});
        }catch(err){
        console.log(err);
        alert("Error: puede que ya este registrado");
      }
      }
  },
//Permite registrar medicos introduciendo el nombre del medico, la cuenta que usara y la contraseña del administrador. Se registrará si no esta registrado ya como paciente ni medico
  registerM: function (nombre,cuenta,pass){
    if(nombre.length>0 && cuenta.length==42){
      try{
      App.contracts.Historial.deployed().then(function(instance){instance.addMedico(nombre,cuenta,pass).then(function(){window.location.replace(ipfs+"/");})});
      }catch(err){
        console.log(err);
        alert("Error: puede que ya este registrado");
      }
    }
  },
//Realiza un control de acceso, si se es medico controla que solo se mueva por el apartado de medicos, si se es pacientes solo en su historial y si no se es nadie, en la entrada y registro
  getQuienEs: function(){
    App.contracts.Historial.deployed().then(function(instance){instance.quienEs.call().then(function(es){
      if (es==2) {
        if (window.location.pathname!=ipfs+"/paciente.html" && window.location.pathname!=ipfs+"/icontra.html") { 
          window.location.replace(ipfs+"/paciente.html");
        }
      }
      else if(es==1){
        if (window.location.pathname!=ipfs+"/medico.html" && window.location.pathname!=ipfs+"/medico_Historial.html") {
          window.location.replace(ipfs+"/medico.html");
        }
      }
      else{
        if (window.location.pathname!=ipfs+"/" && window.location.pathname!=ipfs+"/registro.html" && window.location.pathname!=ipfs+"/iregistro.html") {
          window.location.replace(ipfs+"/");
        }
      }

      
    })

    });
  },
//Devuelve a un medico el historial del paciente con el id que se introduzca y mete la fecha en la que se escribio,la entrada y el nombre del doctor que lo escribio en una tabla
  getHistorial: function(id){
    App.contracts.Historial.deployed().then(function(instance){
      instance.getEntradasH.call(id).then(function(count){
        var i=count-1;

        while (i>0) {
              instance.getHistorial.call(id,i).then(function(h){
              var row = $("<tr />");
              $("<td >"+h[2]+"</>").appendTo(row);
              $("<td >"+h[0]+"</>").appendTo(row);
              $("<td >"+h[1]+"</>").appendTo(row);
              row.appendTo('#tablehistorialpac');
              
            });
            i--;
          }
      })

        //opciones de la tabla
      var dtable=$('#tablehistorialpac').DataTable({
        "bJQueryUI":true,
        "sPaginationType":"full_numbers",
        "ordering": false,
        "bFilter": false,
        "columns": [
          { "width": "15%" },
          { "width": "65%" },
          { "width": "20%" },
        ]
      });

      $('#tablehistorialpac > tbody > tr:first-child').remove();
    }).catch(function(err){console.log(err);alert("Error");});
  },
//Le devuelve al paciente que lo ejecuta su historial y lo introduce en una tabla. Solo lo ejecutan pacientes
  getHistorialP: function(){
    App.contracts.Historial.deployed().then(function(instance){
      instance.getEntradasH.call(0).then(function(count){
        var i=count-1;

        while (i>0) {
              instance.getHistorialP.call(i).then(function(h){
              var row = $("<tr />");
              $("<td >"+h[2]+"</>").appendTo(row);
              $("<td >"+h[0]+"</>").appendTo(row);
              $("<td >"+h[1]+"</>").appendTo(row);
              row.appendTo('#tablehistorialpac');
              
            });
            i--;
          }
      })

        //opciones de la tabla
      var dtable=$('#tablehistorialpac').DataTable({
        "bJQueryUI":true,
        "sPaginationType":"full_numbers",
        "ordering": false,
        "bFilter": false,
        "columns": [
          { "width": "15%" },
          { "width": "65%" },
          { "width": "20%" },
        ]
      });

      $('#tablehistorialpac > tbody > tr:first-child').remove();
    }).catch(function(err){console.log(err);alert("Error");});

    
  },
//Devuelve al paciente su nombre, id y fecha de nacimiento. Solo lo ejecutan los pacientes
  getPacientesP: function(){
    App.contracts.Historial.deployed().then(function(instance){instance.getPacientes.call(0).then(function(i){
      $("#ident").text("ID: "+i[0]);
      $("#fech").text("Fecha de nacimiento: "+i[2]);
      $("#nom").text("Nombre completo: "+i[1]);
    })});
  },
//Le devuelve el nombre, fecha de nacimiento e id de un paciente que se le pase(id). Solo lo ejecuta un medico si id>=1
  getPaciente: function(id){
    App.contracts.Historial.deployed().then(function(instance){instance.getPacientes.call(id).then(function(i){
      $("#ident").text("ID: "+i[0]);
      $("#fech").text("Fecha de nacimiento: "+i[2]);
      $("#nom").text("Nombre completo: "+i[1]);
    })});
  },
//Consigue todos los pacientes registrados en la aplicacion e introduce su nombre, id y fecha de nacimiento en una tabla. Solo lo puede ejecutar un medico
  getPacientesM: function(){
    App.contracts.Historial.deployed().then(function(instance){
        instance.numpacientes().then(function(count){
        var i=count;
        while (i>0) {
              instance.getPacientes.call(i).then(function(h){
              var row = $("<tr id="+i+"/>");
              $('<td onclick="location.href='+"'"+ipfs+"/medico_Historial.html?i="+h[0]+"'"+'">'+h[0]+"</>").appendTo(row);
              $('<td onclick="location.href='+"'"+ipfs+"/medico_Historial.html?i="+h[0]+"'"+'">'+h[1]+"</>").appendTo(row);
              $('<td onclick="location.href='+"'"+ipfs+"/medico_Historial.html?i="+h[0]+"'"+'">'+h[2]+"</>").appendTo(row);
              $('<td onclick="location.href='+"'"+ipfs+"/medico_Historial.html?i="+h[0]+"'"+'">Click para entrar al historial'+"</>").appendTo(row);
              row.appendTo('#tablepac');
            });
            i--;
          }
      })

        //opciones de la tabla
      var dtable=$('#tablepac').DataTable({
        "bJQueryUI":true,
        "sPaginationType":"full_numbers",
        "ordering": false,
        "bFilter": false,
        "columns": [
          { "width": "10%" },
          { "width": "30%" },
          { "width": "30%" },
          { "width": "30%" }
        ]
      });

      $('#tablepac > tbody > tr:first-child').remove();
    }).catch(function(err){console.log(err);alert("");});
  },
// Devuelve el pass que tiene el paciente, solo lo puede ejecutar un paciente y le devuelve su pass
  getPacientePass: function(){
    App.contracts.Historial.deployed().then(function(instance){instance.getPacientePass.call().then(function(p){
      $("#contra").text(p);
    })});
  },
// Anade una entrada al historial introduciendo el identificador del paciente, la entrada y el password que el paciente tiene, solo lo puede ejecutar un medico
  addHistorial: function(id,ent, pass){
    var fecha= new Date();
    App.contracts.Historial.deployed().then(function(instance){instance.addToHistorial(id,fecha.getDate()+"-"+(fecha.getMonth()+1)+"-"+fecha.getFullYear(),ent,pass).then(function(){window.location.replace(ipfs+"/medico.html");})});
  }


  };
// Inicia toda la información en cada ventana
$(function() {
  $(window).load(function() {
    App.init();
  });
});
