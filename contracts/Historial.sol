pragma solidity ^0.4.24;
//IMPORTANTE CAMBIAR PASS_ADMINISTRADOR ABAJO

contract Historial {
    //estructura de paciente de la que se necesita un identificador, nombrem fecha de nacimiento, contrasena privada, y los datos del historial(entradas, fecha en la que se escribio y medico que lo escribio)
    struct Paciente {
        uint id;
        string nombre;
        string fechan;
        string pass;
        string[] history;
        string[] fecha;
        string[] docname;
    }
    //estructura de medico de la que solo se necesita el nombre
    struct Medico {
        string nombre;
    }
    //lista de cuentas asociadas a pacientes
    mapping(address => Paciente) private pacientes;
    //es la lista de cuentas asociadas a medicos
    mapping(address => Medico) private medicos;
    //es la lista de los ids de los pacientes asociada a las cuentas de los pacientes
    mapping(uint => address) private listaPacientes;
    //contiene el numero de pacientes
    uint public numpacientes;

    constructor () public {
        
    }
//añade un paciente con su nombre, fecha de nacimiento y contraseña, ademas inicializa todos los demas atributos de esa estructura
    function addPaciente (string _nombre, string _fechan,string _pass) public {
            require(pacientes[msg.sender].id<1);
            require(keccak256(medicos[msg.sender].nombre)==keccak256(""));
            numpacientes ++;
            pacientes[msg.sender].id = numpacientes; 
            pacientes[msg.sender].nombre = _nombre;
            pacientes[msg.sender].fechan = _fechan;
            pacientes[msg.sender].pass = _pass;
            pacientes[msg.sender].history.push("init");
            pacientes[msg.sender].fecha.push("init");
            pacientes[msg.sender].docname.push("init");
            listaPacientes[numpacientes] = msg.sender;

    }
//añade un medico con nombre _nombre y cuenta _cuenta si la pass es igual a la del administrador y si no esta registrada la cuenta como medico o paciente
    function addMedico (string _nombre, address _cuenta, string _pass) public {
        require(keccak256(_pass)==keccak256("*PASS_ADMINISTRADOR*"));
        require(pacientes[_cuenta].id<1);
        require(keccak256(medicos[_cuenta].nombre)==keccak256(""));
        medicos[_cuenta].nombre = _nombre;
    }
//devuelve el numero de entradas que tiene el historial del paciente con id=_id, si es paciente _id sera 0 y si es medico _id sera mayor que 0 y menor que el numero de pacientes
    function getEntradasH(uint _id) public returns(uint){
        if(_id==0){
            require(keccak256(pacientes[msg.sender].nombre)!=keccak256(""));
            return (pacientes[msg.sender].history.length);
        }
        if(_id>0 && _id<=numpacientes){
            require(keccak256(medicos[msg.sender].nombre)!=keccak256(""));
            return (pacientes[listaPacientes[_id]].history.length);
        }
    }
//anade una entrada al historial del paciente con id=id si la contrasena es _pass
    function addToHistorial(uint _idPaciente, string _fecha, string _entrada, string _pass) public {
        require(keccak256(medicos[msg.sender].nombre)!=keccak256(""));
        require(keccak256(pacientes[listaPacientes[_idPaciente]].pass)==keccak256(_pass));
        pacientes[listaPacientes[_idPaciente]].history.push(_entrada);
        pacientes[listaPacientes[_idPaciente]].fecha.push(_fecha);
        pacientes[listaPacientes[_idPaciente]].docname.push(medicos[msg.sender].nombre);
    }
//devuelve a un medico la entrada numero i del historial del paciente con id=_idPaciente
    function getHistorial(uint _idPaciente, uint i) public returns (string history, string docname, string fecha){
        if( pacientes[listaPacientes[_idPaciente]].history.length>i && keccak256(medicos[msg.sender].nombre)!=keccak256("")){
            return (pacientes[listaPacientes[_idPaciente]].history[i],pacientes[listaPacientes[_idPaciente]].docname[i],pacientes[listaPacientes[_idPaciente]].fecha[i]);
        }
        return("finish","finish","finish");
    }
//le devuelve al paciente que lo llame la entrada del historial que se especifica en el numero i
    function getHistorialP(uint i) public returns (string history, string docname, string fecha){
        if(pacientes[msg.sender].history.length>i && keccak256(pacientes[msg.sender].nombre)!=keccak256("")){
            return (pacientes[msg.sender].history[i],pacientes[msg.sender].docname[i],pacientes[msg.sender].fecha[i]);
        }
        return("finish","finish","finish");
    }
//devuelve los datos (nombre, id y fecha de nacimiento) de un paciente del que se especifica el id, si el id es 0 es un paciente llamando al metodo y le devolvera sus datos, si es mayor que 0 es un medico pidiendo datos de un paciente
    function getPacientes(uint i) public returns(uint id, string nombre, string fechan){
        if(i==0){
            require(keccak256(pacientes[msg.sender].nombre)!=keccak256(""));
            return (pacientes[msg.sender].id,pacientes[msg.sender].nombre,pacientes[msg.sender].fechan);
        }
        if(i>0 && i<=numpacientes){
            require(keccak256(medicos[msg.sender].nombre)!=keccak256(""));
            return (pacientes[listaPacientes[i]].id,pacientes[listaPacientes[i]].nombre,pacientes[listaPacientes[i]].fechan);
        }
    }
//devuelve quien es el que llama a la funcion, 1=medico, 2=paciente y 0=nadie
    function quienEs() public returns(uint){
        if(keccak256(medicos[msg.sender].nombre)!=keccak256("")){
            return (1);
        }
        if(keccak256(pacientes[msg.sender].nombre)!=keccak256("")){
            return (2);
        }
        return (0);
    }
//devuelve al paciente que llame al metodo su contraseña
    function getPacientePass() public returns(string){
        require(keccak256(pacientes[msg.sender].nombre)!=keccak256(""));
        return (pacientes[msg.sender].pass);
    }

}
